
 //
//  UtilityClass.swift
//  MyanCareDoctor
//
//  Created by Santosh on 19/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import UIKit
import SafariServices


let userDefault = UserDefaults.standard

private enum UserDefaultEnum : String {
    case userInfoData = "userInfoData"
    case welcomeScreen = "welcomeScreen"
    case fcmDeviceToken = "FCM_Token"
}

class UtilityClass: NSObject {
    
    class func getCommaSepratedStringFromArray(completeArray arr : NSMutableArray) -> String {
        
        var nameArr: [String] = []
        
        for i in 0 ..< arr.count
        {
            let nameStr = arr.object(at: i) as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    //MARK: User Info ( DELETE )
    class func deleteUserData() -> Void
    {
        guard userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) != nil else
        {
            return
        }
        
        userDefault.removeObject(forKey: UserDefaultEnum.userInfoData.rawValue)
        
//        modelChangePasswordProcess = ChangePasswordModel.init()
//        modelEditProfileProcess = EditProfileModel.init()
//        modelSignUpProcess = ModelLogin.init()
    }
    
    //MARK: - Take a Screenshot
    class func screenShotMethod() -> UIImage {
        
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
        
    }
    
    class func getDateStringFromTimeStamp(timeStamp : String, dateFormat: String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10 {
            
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = dateFormat //Specify your format that you want
        
        dateFormatter.pmSymbol = "PM"
        dateFormatter.amSymbol = "AM"
        
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }

    class func getDateFromString (date : String, formate : String) -> Date
    {
        //26/01/2018
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate //Your date format
        let date1 = dateFormatter.date(from: date) //according to date format your date string
        print(date) //Convert String to Date
        
        return date1!
    }
    
    class func getStringFromDate (date : Date, formate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: date)
        let yourDate: Date? = formatter.date(from: myString)
        formatter.dateFormat = formate
        let updatedString = formatter.string(from: yourDate!)
        
        return updatedString
    }
    
    class func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "mm/dd/yyyy"
        
        return  dateFormatter.string(from: date!)
    }
    
    class func convertDateFormaterDDMMtoMMDD(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/mm/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "mm/dd/yyyy"
        
        return  dateFormatter.string(from: date!)
    }

    
    //MARK:- Change RootViewController
    class func changeRootViewController(with newRootViewController : UIViewController) -> Void
    {
        if let navigationCont = appDelegate?.window??.rootViewController as? UINavigationController
        {
            navigationCont.popToRootViewController(animated: false)
        }
        
        appDelegate?.window??.rootViewController = newRootViewController
    }
    
    class func setStatusBarBGColor() -> Void
    {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.MyanCareDoctor.appDefaultGreenColor
        }
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK:- Get User Age From DOB
    class func getPersonYearOld (date : String) -> Int
    {
        if date == "" {
            return 0
        } else {
            //26/01/2018
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM,yyyy" //Your date format
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
            let date1 = dateFormatter.date(from: date) //according to date format your date string
            
            let calendar = Calendar.current
            
            let components : DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.year], from: date1!, to: Date(), options: NSCalendar.Options())
            
            return components.year!
        }
    }
    
    //MARK:- Get Comma Seprated String From Array
    class func getCommaSepratedStringFromArrayDict(completeArray:[[String:Any]], withKeyName key:String) -> String
    {
        var nameArr: [String] = []
        
        for name in completeArray
        {
            let nameStr = name[key] as! String
            nameArr.append(nameStr)
        }
        
        let commaSeparatedNameString = nameArr.joined(separator: ",")
        
        return commaSeparatedNameString
    }
    
    //MARK:- User Info ( SAVE )
    class func saveUserInfoData(userDict : [String:Any]) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefault.set(data, forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    //MARK:- User Info ( GET )
    class func getUserInfoData() -> [String:Any]
    {
        let data : Data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        return userDict
    }
    
    //MARK:- set wallet balave into User Info ( SET )
    class func setWalletBalanceInUserInfo(walletBalance : Int)
    {
        let data : Data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        var userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        userDict["wallet_balance"] = walletBalance
        
        let data1 = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefault.set(data1, forKey: UserDefaultEnum.userInfoData.rawValue)
    }
    
    //MARK: User Sid ( GET )
    class func getUserAccessKeyData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
     
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        var strSid = userDict["access_key"] as? String
        
        guard strSid != nil else {
            strSid = ""
            return strSid
        }
        
        return strSid
}
    
    //MARK: User ID ( GET )
    class func getUserIdData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["id"] as! String
        
        return strSid
    }
    
    //MARK: User Store ID ( GET )
    class func getUserStoreIdData() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as? Data else
        {
            return nil
        }
        
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        let strSid = userDict["store_id"] as! String
        
        return strSid
    }
    
//MARK: Save Notification Status ( Save )
    class func saveWelcomeScreenStatus(userDict : Bool) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefault.set(data, forKey: UserDefaultEnum.welcomeScreen.rawValue)
        
    }
    
    //MARK: Get Notification Status ( GET )
    class func getWelcomeScreenStatus() -> Bool?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.welcomeScreen.rawValue) as? Data else
        {
            return nil
        }
        
        let userDict : Bool = NSKeyedUnarchiver.unarchiveObject(with: data) as! Bool
        
        
        return userDict
    }
    
    //MARK: Save Notification Status ( Save )
    class func saveDeviceFCMToken(userDict : String) -> Void
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: userDict)
        userDefault.set(data, forKey: UserDefaultEnum.fcmDeviceToken.rawValue)
    }
    
    //MARK: Get Notification Status ( GET )
    class func getDeviceFCMToken() -> String?
    {
        guard let data = userDefault.object(forKey: UserDefaultEnum.fcmDeviceToken.rawValue) as? Data else
        {
            return nil
        }
        
        let userDict : String = NSKeyedUnarchiver.unarchiveObject(with: data) as! String
        
        return userDict
    }
    
    //MARK: User ID ( GET )

    class func getDateStringFromTimeStamp(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10 {
            
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        
        //dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd-MMM-yyyy | hh:mm a" //Specify your format that you want
        
        let strDate = dateFormatter.string(from: date)
       
        return strDate as NSString
    }
    
   
    
    
    //MARK:- Extract Number
    class func extractNumber(fromString string:String) -> String
    {
        let characterSet = CharacterSet.decimalDigits.inverted
        let stringArray = string.components(separatedBy: characterSet)
        let newString = stringArray.joined(separator: "")
        
        return newString
    }
    
 
    
    //MARK:- Alert View
    static func showAlertWithTitle(title:String, isDissmissShow:Bool, message:String? = App_Global_Msg, onViewController:UIViewController?, withButtonArray buttonArray:[String]? = [], dismissHandler:((_ buttonIndex:Int)->())?) -> Void {
        
        let alertController = UIAlertController(title: App_Name, message: message, preferredStyle: .alert)
        
        var ignoreButtonArray = false
        
        if buttonArray == nil
        {
            ignoreButtonArray = true
        }
        
        if !ignoreButtonArray
        {
            for item in buttonArray!
            {
                let action = UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                    alertController.dismiss(animated: true, completion: nil)
                    
                    guard (dismissHandler != nil) else
                    {
                        return
                    }
                    dismissHandler!(buttonArray!.index(of: item)!)
                })
                alertController.addAction(action)
            }
        }
        
        let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: { (action) in
            
            guard (dismissHandler != nil) else
            {
                return
            }
            dismissHandler!(LONG_MAX)
        })
        if isDissmissShow
        {
        alertController.addAction(action)
        }
        
        
        onViewController?.present(alertController, animated: true, completion: nil)
        
    }
    
    //MARK:-
    //MARK:- Open Safari ViewController as A WebView
    class func openSafariController(usingLink openLink:LinksEnum, onViewController: UIViewController?) -> Void
    {
        guard let visibleController = onViewController else {
            return
        }
        
        let safariController : SFSafariViewController = SFSafariViewController(url: URL(string: openLink.rawValue)!)
        safariController.delegate = visibleController as? SFSafariViewControllerDelegate
        visibleController.present(safariController, animated: true, completion: nil)
    }
    
    class func openSafariBrowser (usingLink urlLink : LinksEnum) -> Void
    {
        let openUrl = URL(string: urlLink.rawValue)
        
        let application:UIApplication = UIApplication.shared
            
        if (application.canOpenURL(openUrl!))
        {
            if #available(iOS 10.0, *)
            {
                application.open(openUrl!, options: [:], completionHandler: nil)
            }
            else
            {
                // Fallback on earlier versions
                print("other versions")
                application.openURL(openUrl!)
            }
        }
        else
        {
            print("can not open url")
        }
    }
    
    class func setPasscode (passcodString : String) {
        UserDefaults.standard.set(passcodString, forKey: "passcode_string")
        UserDefaults.standard.synchronize()
    }
    
    class func getPasscode () -> String {
        
        if UserDefaults.standard.object(forKey: "passcode_string") != nil {
            return UserDefaults.standard.object(forKey: "passcode_string") as! String
        } else {
            return ""
        }
    }
    
    class func enableTouchIDOrNot (enable : Bool) {
        UserDefaults.standard.set(enable, forKey: "set_touchID")
        UserDefaults.standard.synchronize()
    }
    
    class func getEnableTouchIDOrNot () -> Bool {
        if UserDefaults.standard.object(forKey: "set_touchID") != nil {
            return UserDefaults.standard.object(forKey: "set_touchID") as! Bool
        } else {
            return false
        }
    }
    
    class func getPersonName() -> String {
        
        let data : Data = userDefault.object(forKey: UserDefaultEnum.userInfoData.rawValue) as! Data
        let userDict : [String:Any] = NSKeyedUnarchiver.unarchiveObject(with: data) as! [String : Any]
        
        let name = userDict["name"] as! String
        
        return name
    }
    
    class func setPasscodeOnOff (passcodeOnOff : Bool) {
        UserDefaults.standard.set(passcodeOnOff, forKey: "passcode_on_off")
        UserDefaults.standard.synchronize()
    }
    
    class func getPasscodeOnOff () -> Bool {
        
        if UserDefaults.standard.object(forKey: "passcode_on_off") != nil {
            return UserDefaults.standard.object(forKey: "passcode_on_off") as! Bool
        } else {
            return false
        }
    }
    
    class func timeAgoSinceDate(_ date:Date, currentDate:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(date)
        let latest = (earliest == now) ? date : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2)
        {
            return "\(components.year!) years ago"
        }
        else if (components.year! >= 1)
        {
            if (numericDates)
            {
                return "1 year ago"
            }
            else
            {
                return "Last year"
            }
        }
        else if (components.month! >= 2)
        {
            return "\(components.month!) months ago"
        }
        else if (components.month! >= 1)
        {
            if (numericDates)
            {
                return "1 month ago"
            }
            else
            {
                return "Last month"
            }
        }
        else if (components.weekOfYear! >= 2)
        {
            return "\(components.weekOfYear!) weeks ago"
        }
        else if (components.weekOfYear! >= 1)
        {
            if (numericDates)
            {
                return "1 week ago"
            }
            else
            {
                return "Last week"
            }
        }
        else if (components.day! >= 2)
        {
            return "\(components.day!) days ago"
        }
        else if (components.day! >= 1)
        {
            if (numericDates)
            {
                return "1 day ago"
            }
            else
            {
                return "Yesterday"
            }
        }
        else if (components.hour! >= 2)
        {
            return "\(components.hour!) hours ago"
        }
        else if (components.hour! >= 1)
        {
            if (numericDates)
            {
                return "1 hour ago"
            }
            else
            {
                return "An hour ago"
            }
        }
        else if (components.minute! >= 2)
        {
            return "\(components.minute!) minutes ago"
        }
        else if (components.minute! >= 1)
        {
            if (numericDates)
            {
                return "1 minute ago"
            }
            else
            {
                return "A minute ago"
            }
        }
        else if (components.second! >= 3)
        {
            return "\(components.second!) seconds ago"
        }
        else
        {
            return "Just now"
        }
    }
    
    class func getDateStringFromTimeStamp1(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10
        {
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017
        dateFormatter.dateFormat = "dd-MMM-yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
    
    //MARK:- get timeStamp from current data
    class func getCurrentTimeStamp () -> String
    {
        // Get the Unix timestamp
        let timestamp = Date().timeIntervalSince1970
        print(timestamp)
        
        return String(Int(timestamp))
    }
    
    class func getDateStringFromTimeStamp2(timeStamp : String) -> NSString
    {
        var time = timeStamp
        
        if (time.count) > 10 {
            
            let index2 = (time.index((time.startIndex), offsetBy: 10))
            let indexStart = index2
            
            let indexEnd = (time.endIndex)
            
            time.removeSubrange(indexStart ..< indexEnd)
        }
        
        let date = Date(timeIntervalSince1970: Double(time)! )
        let dateFormatter = DateFormatter()
        //    dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        
        //12-Sep-2017 | 10:00 AM
        dateFormatter.dateFormat = "dd MMM,yyyy" //Specify your format that you want
        let strDate = dateFormatter.string(from: date)
        
        return strDate as NSString
    }
}


//MARK: -
//MARK: - extension -> Documents Directory Handling
extension UtilityClass
{
    class func getDocumentsDirectoryPath() -> URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    class func getDocumentsFolder(withName folderName: String) -> URL? {
        let documentsPath = UtilityClass.getDocumentsDirectoryPath()
        print(documentsPath)
        
        let newDirectoryPath = documentsPath.appendingPathComponent(folderName) //documentsPath.appending("/"+folderName)
        
        let fileManager = FileManager.default
        
        if !fileManager.fileExists(atPath: newDirectoryPath.path) {
            do {
                try fileManager.createDirectory(atPath: newDirectoryPath.path, withIntermediateDirectories: true, attributes: nil)
            } catch {
                return nil
            }
        }
        return newDirectoryPath
    }
    
    class func addFileToFolder(_ folder: String, fileName: String, fileData: Data) -> Bool {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return false
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName) //folderDirectory.appending("/"+fileName)
        
        let isSaved = fileManager.createFile(atPath: newFilePath.path, contents: fileData, attributes: nil)
        
        return isSaved
    }
    
    class func getFileURLFromFolder(_ folder: String, fileName: String) -> URL? {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        if fileManager.fileExists(atPath: newFilePath.path) {
            return newFilePath
        }
        return nil
    }
    
    class func getFileFromFolder(_ folder: String, fileName: String) -> Data? {
        guard let folderDirectory = UtilityClass.getDocumentsFolder(withName: folder) else {
            return nil
        }
        
        let fileManager = FileManager.default
        
        let newFilePath = folderDirectory.appendingPathComponent(fileName)
        let newData = fileManager.contents(atPath: newFilePath.path)
        //        if fileManager.fileExists(atPath: newFilePath.path) {
        return newData
        //        }
        //        return nil
}
}
