//
//  UIFontExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit
extension UIFont
{
    //["Poppins-Bold", "Poppins-Light", "Poppins-Medium", "Poppins-SemiBold", "Poppins-Regular"]
    
    static func createPoppinsMediumFont(withSize size: CGFloat) -> UIFont {
        let font = UIFont (name: "Poppins-Medium", size: size)
        return font!
    }
    static func createPoppinsBoldFont(withSize size: CGFloat) -> UIFont {
        let font = UIFont (name: "Poppins-Bold", size: size)
        return font!
    }
    static func createPoppinsLightFont(withSize size: CGFloat) -> UIFont {
        let font = UIFont (name: "Poppins-Light", size: size)
        return font!
    }
    static func createPoppinsSemiBoldFont(withSize size: CGFloat) -> UIFont {
        let font = UIFont (name: "Poppins-SemiBold", size: size)
        return font!
    }
    static func createPoppinsRegularFont(withSize size: CGFloat) -> UIFont {
        let font = UIFont (name: "Poppins-Regular", size: size)
        return font!
    }
    struct MyanCareDoctor
    {
        
    }
}
