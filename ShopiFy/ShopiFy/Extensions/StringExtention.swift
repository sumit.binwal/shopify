//
//  StringExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 09/01/2018.
//  Copyright © 2018 sumit. All rights reserved.
//

import Foundation

extension String
{
    func isEmptyString() -> Bool
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if newString.isEmpty
        {
            return true
        }
        return false
    }
    
    func getTrimmedText() -> String
    {
        let newString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return newString
    }
    
    func isValidEmailAddress() -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        return  returnValue
    }
    
}
