//
//  UIImageViewExtension.swift
//  MyancarePatient
//
//  Created by Jyoti on 05/01/18.
//  Copyright © 2018 konstant. All rights reserved.
//

import Foundation
import UIKit
extension UIImageView {
    
    func roundedImageView() {
        self.layer.cornerRadius = self.frame.size.height/2
        self.layer.masksToBounds = true
    }
    func createBorder(withColor borderColor:UIColor, andBorderWidth borderWidth:CGFloat) -> Void {
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = CGFloat(borderWidth)
    }
}
