//
//  UIStoryBoardExtention.swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 15/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit
extension UIStoryboard
{
    static func getInitialStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "InitialVIews", bundle: nil)
        return storyBoard
    }
    
    static func getMerchantStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Merchant", bundle: nil)
        return storyBoard
    }
    
    static func getLoginStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Login", bundle: nil)
        return storyBoard
    }
    
    static func getLanguageStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Language", bundle: nil)
        return storyBoard
    }
    
    static func getLocationStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Location", bundle: nil)
        return storyBoard
    }
    
    static func getQualificationStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Qualification", bundle: nil)
        return storyBoard
    }
    
    static func getPhoneInfoStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "PhoneInfo", bundle: nil)
        return storyBoard
    }
    
    static func ScheduleAppointmentStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ScheduleAppointment", bundle: nil)
        return storyBoard
    }
    
    static func getChangePhoneNumberStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "ChangePhoneNumber", bundle: nil)
        return storyBoard
    }
    
    static func getHomeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Home", bundle: nil)
        return storyBoard
    }
    
    static func getArticlesStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "Articles", bundle : nil)
        return storyBoard
    }
    
    static func getArticlDetailStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ArticleDetail", bundle : nil)
        return storyBoard
    }
    
    static func getCommentsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Comments", bundle: nil)
        return storyBoard
    }
    
    static func getFilterStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "Category", bundle: nil)
        return storyBoard
    }
    
    static func getBookmarkLikeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name: "BookmarkLike", bundle: nil)
        return storyBoard
    }
    
    static func getAboutUsStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "AboutUs", bundle : nil)
        return storyBoard
    }
    
    static func getWalletStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyWallet", bundle : nil)
        return storyBoard
    }
    
    static func getMyProfileStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyProfile", bundle : nil)
        return storyBoard
    }
    
    static func getChangeTextSizeStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ChangeTextSize", bundle : nil)
        return storyBoard
    }
    
    static func getMoreTabScreensStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MoreTabScreens", bundle : nil)
        return storyBoard
    }
    
    static func getMyRecordStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "MyRecord", bundle : nil)
        return storyBoard
    }
    
    static func getScheduleAppointmentNewStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ScheduleAppointmentNew", bundle : nil)
        return storyBoard
    }
    
    static func getWorkingAddressStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "WorkingAddress", bundle : nil)
        return storyBoard
    }
    
    static func getChatViewStoryBoard() -> UIStoryboard {
        let storyBoard = UIStoryboard (name : "ChatView", bundle : nil)
        return storyBoard
    }
}
