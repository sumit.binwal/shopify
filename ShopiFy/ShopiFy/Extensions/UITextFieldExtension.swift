//
//  UITextFieldExtension.swift
//  MyanCareDoctor
//
//  Created by Santosh on 18/12/17.
//  Copyright © 2017 sumit. All rights reserved.
//

import Foundation
import UIKit

extension UITextField
{
    func setPlaceholderColor(_ color: UIColor)
    {
        self.setValue(color, forKeyPath: "_placeholderLabel.textColor")
    }
}
