//
//  UITableViewCellExtention .swift
//  MyanCareDoctor
//
//  Created by Sumit Sharma on 14/12/2017.
//  Copyright © 2017 sumit. All rights reserved.
//
import UIKit
extension UITableViewCell
{
    func disableSelection() {
        
        self.selectionStyle = .none
    }
}

