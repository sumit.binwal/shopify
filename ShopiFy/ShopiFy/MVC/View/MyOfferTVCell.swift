//
//  MyOfferTVCell.swift
//  ShopiFy
//
//  Created by Rohit Sharma on 4/24/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit

class MyOfferTVCell: UITableViewCell {

    @IBOutlet weak var imgvOfferItem: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblBrandDetail: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSlot: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
