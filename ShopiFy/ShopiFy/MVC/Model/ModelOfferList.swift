//
//  ModelOfferList.swift
//  ShopiFy
//
//  Created by Sumit Sharma on 4/25/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

class ModelOfferList
{
    var category : String?
    var offerID : String?
    var imageURL : String?
    var startDate : String?
    var storeName : String?
    var endDate : String?
    var title : String?
    var type : String?
    
    
    init()
    {
        category = ""
         offerID  = ""
         imageURL  = ""
         startDate  = ""
         storeName  = ""
        endDate = ""
         title  = ""
         type  = ""
        
    }
    
    deinit
    {
        print("ModelOfer Listdeinit")
    }
    
    func updateModel(usingDictionary dictionary:[String:Any]) -> Void
    {
        if let id = dictionary["id"] as? String
        {
            offerID = id
        }
        
        if let category1 = dictionary["category"] as? String
        {
            category = category1
        }
        
        
        
        if let strEndDate = dictionary["date"] as? String
        {
            endDate = strEndDate
        }
        
        if let strImg = dictionary["image"] as? String
        {
            imageURL = strImg
        }
        
        if let strStartDate = dictionary["start-date"] as? String
        {
            startDate = strStartDate
        }
        
        if let strEndDate = dictionary["end-date"] as? String
        {
            endDate = strEndDate
        }
        
        if let strStore = dictionary["store"] as? String
        {
            storeName = strStore
        }
        if let strTitle = dictionary["title"] as? String
        {
            title = strTitle
        }
        
        if let strType = dictionary["type"] as? String
        {
            type = strType
        }
        
       
    }
    
   
}
