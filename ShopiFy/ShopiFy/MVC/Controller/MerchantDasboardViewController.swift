//
//  MerchantDasboardViewController.swift
//  ShopiFy
//
//  Created by Rohit Sharma on 4/24/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit

class MerchantDasboardViewController: UIViewController {

    @IBOutlet weak var btnAllOffer: UIButton!
    @IBOutlet weak var btnAddOffer: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeUI()
        
        self.btnAddOffer.centerImageAndButton(5.0, imageOnTop: true)
        self.btnAllOffer.centerImageAndButton(5.0, imageOnTop: true)
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: Navigation Bar Method
    func initializeUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 241/255, green: 90/255, blue: 37/255, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: leftBarView())
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarView())
        self.navigationItem.hidesBackButton = true
    }
    
    func leftBarView() -> UIView {
        let leftBarView = UIView()
        leftBarView.backgroundColor = UIColor.clear
        
        let buttonLeftView = UIButton(type: .custom)
        buttonLeftView.setTitle(UtilityClass.getUserInfoData()["username"] as? String, for: .normal)
        buttonLeftView.setImage(#imageLiteral(resourceName: "user"), for: .normal)
        buttonLeftView.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17.0)
        leftBarView.addSubview(buttonLeftView)
        
        leftBarView.translatesAutoresizingMaskIntoConstraints = false
        buttonLeftView.translatesAutoresizingMaskIntoConstraints = false

        leftBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonLeftView]))
        leftBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonLeftView]))
        
        return leftBarView
    }
    
    func rightBarView() -> UIView {
        let rightBarView = UIView()
        rightBarView.backgroundColor = UIColor.white
        
        let buttonRightView = UIButton(type: .custom)
        buttonRightView.setImage(#imageLiteral(resourceName: "logout"), for: .normal)
        
        buttonRightView.addTarget(self, action: #selector(logouBtnTap), for: .touchUpInside)
        
        rightBarView.addSubview(buttonRightView)
        
        rightBarView.translatesAutoresizingMaskIntoConstraints = false
        buttonRightView.translatesAutoresizingMaskIntoConstraints = false
        
        rightBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonRightView]))
        rightBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonRightView]))
        
        return rightBarView
    }
    
    
    
    //MARK: IBActions
    @objc func logouBtnTap()
    {
        UtilityClass.showAlertWithTitle(title: App_Name, isDissmissShow: false, message: "Do you really want to logout?", onViewController: self, withButtonArray: ["NO","YES"], dismissHandler: { (buttonIndex) in
            
                        if buttonIndex == 1
                        {
                            UtilityClass.deleteUserData()
                            let loginVC = UIStoryboard.getLoginStoryBoard().instantiateInitialViewController()
                            UtilityClass.changeRootViewController(with: loginVC!)
                        }
            
                    })

        
        
    }
    
    
    @IBAction func addOffer(_ sender: Any) {
        
    }
    
    @IBAction func allOffer(_ sender: Any) {
        self.performSegue(withIdentifier: "MyOfferListScene", sender: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
