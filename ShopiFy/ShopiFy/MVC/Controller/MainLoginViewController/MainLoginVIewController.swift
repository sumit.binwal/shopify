//
//  MainLoginVIewController.swift
//  ShopiFy
//
//  Created by Sumit Sharma on 4/26/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit

class MainLoginVIewController: UIViewController {

    @IBOutlet var vwBorder: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        //Add Border To View
        vwBorder.layer.addBorder(edge: .left, color: .gray, thickness: 0.5)
        vwBorder.layer.addBorder(edge: .top, color: .gray, thickness: 0.5)
        vwBorder.layer.addBorder(edge: .right, color: .gray, thickness: 0.5)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
