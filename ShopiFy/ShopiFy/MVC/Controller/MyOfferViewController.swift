//
//  MyOfferViewController.swift
//  ShopiFy
//
//  Created by Rohit Sharma on 4/24/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit
import PKHUD

class MyOfferViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var arrOfferData = [ModelOfferList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "MyOfferTVCell", bundle: nil), forCellReuseIdentifier: "MyOfferTVCell")
        
        initializeUI()
        self.tableView.separatorStyle = .none
        self.tableView.estimatedRowHeight = 105.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.fetchOffersFromServer()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Navigation Bar Method
    func initializeUI() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 241/255, green: 90/255, blue: 37/255, alpha: 1.0)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBarView())
        self.navigationItem.title = "My Offer"
        
    }
    
    func leftBarView() -> UIView {
        let leftBarView = UIView()
        leftBarView.backgroundColor = UIColor.clear
        
        let buttonLeftView = UIButton(type: .custom)
        buttonLeftView.setTitle("User", for: .normal)
        buttonLeftView.setImage(#imageLiteral(resourceName: "user"), for: .normal)
        buttonLeftView.titleLabel?.font = UIFont(name: "HelveticaNeue-Bold", size: 17.0)
        leftBarView.addSubview(buttonLeftView)
        
        leftBarView.translatesAutoresizingMaskIntoConstraints = false
        buttonLeftView.translatesAutoresizingMaskIntoConstraints = false
        
        leftBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonLeftView]))
        leftBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonLeftView]))
        
        return leftBarView
    }
    
    func rightBarView() -> UIView {
        let rightBarView = UIView()
        rightBarView.backgroundColor = UIColor.white
        
        let buttonRightView = UIButton(type: .custom)
        buttonRightView.setImage(#imageLiteral(resourceName: "logout"), for: .normal)
        rightBarView.addSubview(buttonRightView)
        
        rightBarView.translatesAutoresizingMaskIntoConstraints = false
        buttonRightView.translatesAutoresizingMaskIntoConstraints = false
        
        rightBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonRightView]))
        rightBarView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view":buttonRightView]))
        
        return rightBarView
    }
    
    //MARK: TableView Delegate and DataSource Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfferData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOfferTVCell", for: indexPath) as! MyOfferTVCell
        
        cell.selectionStyle = .none
        cell.lblTitle.text = arrOfferData[indexPath.row].title
        cell.lblCategory.text = "Category : " + arrOfferData[indexPath.row].category!
        cell.lblBrandDetail.text = arrOfferData[indexPath.row].storeName!
        cell.lblSlot.text = arrOfferData[indexPath.row].startDate! + "to" + arrOfferData[indexPath.row].endDate!
        cell.imgvOfferItem.sd_setImage(with: URL(string: arrOfferData[indexPath.row].imageURL!), placeholderImage: UIImage(named: "defaultImg"))

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let offerDetailVC = UIStoryboard.getMerchantStoryBoard().instantiateViewController(withIdentifier: "offerDetail") as! OfferDetailViewController
        offerDetailVC.strOfferID = arrOfferData[indexPath.row].offerID!
        self.navigationController?.pushViewController(offerDetailVC, animated: true)
    }
    
    
    //Call Offer APi
    
    func fetchOffersFromServer()  {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        
        let urlToHit =  EndPoints.fetchOffer(UtilityClass.getUserIdData()!).path
        print(urlToHit)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil) {[unowned self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            if error != nil {
                UtilityClass.showAlertWithTitle(title: App_Name, isDissmissShow: true, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil {
                UtilityClass.showAlertWithTitle(title: App_Name, isDissmissShow: true, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 404 {
                let responseDictionary = dictionary!
                _ = responseDictionary["status"] as! String
                
                UtilityClass.showAlertWithTitle(title: "", isDissmissShow: true, message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary as! [String:Any]
                
                
                guard responseDictionary != nil else
                {
                    return
                }
                
                let error = responseDictionary["error"] as! Bool
                
                
                if !error {
                    let appointmentDtaArr1 = responseDictionary["offer"] as? [[String : Any]]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                    self.updateModelArray(usingArray: appointmentDtaArr1!)
                    
                } else {
                    
                    UtilityClass.showAlertWithTitle(title: "", isDissmissShow: true, message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
        }
    }
    
    //MARK:- Updating Model Array
    func updateModelArray(usingArray array : [[String:Any]]) -> Void
    {
        arrOfferData.removeAll()
        
        let dataArray = array
        
        for dict in dataArray // Iterating dictionaries
        {
            let model = ModelOfferList () // Model creation
            model.updateModel(usingDictionary: dict) // Updating model
            arrOfferData.append(model) // Adding model to array
        }
        
        tableView.reloadData()
    }
}
