//
//  GetStartedNotificationVC.swift
//  ShopiFy
//
//  Created by Sumit Sharma on 4/26/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit

class GetStartedNotificationVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func gotItButtonClickedAction(_ sender: Any)
    {
        let merchntVw = UIStoryboard.getLoginStoryBoard().instantiateViewController(withIdentifier: "mainLoginVC") as! MainLoginVIewController
        navigationController?.pushViewController(merchntVw, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
