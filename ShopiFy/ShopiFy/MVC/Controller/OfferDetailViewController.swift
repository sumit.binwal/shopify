//
//  OfferDetailViewController.swift
//  ShopiFy
//
//  Created by Sumit Sharma on 4/26/18.
//  Copyright © 2018 Sumit Sharma. All rights reserved.
//

import UIKit
import PKHUD

class OfferDetailViewController: UIViewController {

    @IBOutlet var imgVwOffer: UIImageView!
    @IBOutlet var lblOfferLocation: UILabel!
    @IBOutlet var lblOfferTitle: UILabel!
    @IBOutlet var lblOfferTime: UILabel!
    @IBOutlet var lblOfferCategory: UILabel!
    @IBOutlet var lblOfferDiscription: UILabel!
    var offerDetailModel:ModelOfferList = ModelOfferList()
    var strOfferID:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchOffersDetailsFromServer()
        
        imgVwOffer.sd_setImage(with: NSURL.init(string: offerDetailModel.imageURL!) as! URL, completed: nil)
        lblOfferTitle.text = offerDetailModel.title
        lblOfferTime.text = offerDetailModel.startDate
        lblOfferCategory.text = offerDetailModel.category
        lblOfferLocation.text = offerDetailModel.storeName
        lblOfferDiscription.text = offerDetailModel.title
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //Call Offer APi
    
    func fetchOffersDetailsFromServer()  {
        HUD.show(.systemActivity, onView: self.view.window)
        
        self.view.endEditing(true)
        
        
        let urlToHit =  EndPoints.fetchOfferDetail(strOfferID, UtilityClass.getUserIdData()!, UtilityClass.getUserAccessKeyData()!).path
        print(urlToHit)
        
        AppWebHandler.sharedInstance().fetchData(fromURL: urlToHit, httpMethod: .get, parameters: nil) {[unowned self] (data, dictionary, statusCode, error) in
            
            HUD.hide()
            
            if error != nil {
                UtilityClass.showAlertWithTitle(title: App_Name, isDissmissShow: true, message: error!.localizedDescription, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if dictionary == nil {
                UtilityClass.showAlertWithTitle(title: App_Name, isDissmissShow: true, message: App_Global_Error_Msg, onViewController: self, withButtonArray: nil, dismissHandler: nil)
                
                return
            }
            
            if statusCode == 404 {
                let responseDictionary = dictionary!
                _ = responseDictionary["status"] as! String
                
                UtilityClass.showAlertWithTitle(title: "", isDissmissShow: true, message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                    
                })
            }
            
            if statusCode == 200
            {
                let responseDictionary = dictionary as! [String:Any]
                
                
                guard responseDictionary != nil else
                {
                    return
                }
                
                let error = responseDictionary["error"] as! Bool
                
                
                if !error {
                    let appointmentDtaArr1 = responseDictionary["offer"] as? [[String : Any]]
                    
                    guard appointmentDtaArr1 != nil else
                    {
                        return
                    }
                    
                  //  self.updateModelArray(usingArray: appointmentDtaArr1!)
                    
                } else {
                    
                    UtilityClass.showAlertWithTitle(title: "", isDissmissShow: true, message:responseDictionary["msg"] as? String , onViewController: self, withButtonArray: nil, dismissHandler: { (buttonIndex) in
                        
                    })
                }
            }
        }
    }

}
