//
//  API.swift
//  OOTTBusinessApp
//
//  Created by Sumit on 03/01/17.
//  Copyright © 2017 KonstantInfoSolutions. All rights reserved.
//

import Foundation

//MARK:- DEVELOPMENT BASE URL
let baseURLString = "https://www.shoppyfier.com/api/"

//MARK:- STAGING BASE URL
//let baseURLString = "http://52.42.40.118:15001/"


//MARK:- CLIENT BASE URL
//let baseURLString = ""

//MARK:- Protocol Endpoint path
protocol ApiEndpoint {
    var path :URL {get}
}

//MARK:- Enum Endpoint Final Path
enum EndPoints : ApiEndpoint {
    
    case merchantLogin
    case fetchOffer(String)
    case fetchOfferDetail(String,String,String)
    
    
    
    case register
    case resendOTP
    case getNearest(String,String)
    case specialization
    case verifyOTP
    case checkUserExist
    case changeMobileNumber
    case forgotPassword
    case resetPassword
    case verifyForgotOtp
    case categoryList
    case articles
    case articleListByCat(String)
    case articleLike
    case articleView
    case appointmentList(String,String)
    
    case get_comment(String)
    case comment
    case search(String, String)

    case myLikes
    case myBookmark
    
    case articleBookMark
    
    case rejectAppointment
    case acceptAppointment
    
    case addToWallet
    
    case notification
    
    case profile
    case update_profile
    case change_password
    
    case myTransactions
    
    case doctorReviewGet(String)
    
    case getShareDocs
    
    case patient_list
    
    case operatinghours(String, String)
    
    case updateoperatinghours
    
    case getDaySlots
    
    case getRecordFiles(String)
    case uploadRecordFile
    
    case medicalRecordsAll
    case medicalRecords
    case createMedicalRecords
    case medicalRecordShareWith

    case getDoctorWorkingAddress(String)
    case doctorWorkingAddress
    
    case logoutUser
    case updateDeviceToken

    var path: URL
    {
        switch self {

        case .merchantLogin:
            return URL(string: String(baseURLString+"merchant/login"))!
            
        case .fetchOffer(let userId):
            return URL(string: String(baseURLString+"merchant/offers/?userid="+userId))!

        case .fetchOfferDetail(let offerID, let userId, let accessKey):
                
                return URL(string: String(baseURLString+"merchant/offerDetail/?userid="+String(userId)+"%offerid="+String(offerID)+"%accesskey="+accessKey))!
            
        case .doctorWorkingAddress:
            return URL(string: String(baseURLString+"doctorWorkingAddress"))!

        case .updateDeviceToken:
            return URL(string: String(baseURLString+"user/updateFcmToken"))!
            
        case .logoutUser:
            return URL(string: String(baseURLString+"auth/logout"))!

            
        case .getDaySlots:
            return URL(string: String(baseURLString+"auth/getDaySlots"))!
            
        case .updateoperatinghours:
                return URL(string: String(baseURLString+"user/update_operating_hours"))!
            
        case .operatinghours(let doctor_id, let dateString):
            return URL(string: String(baseURLString+"operatinghours/"+doctor_id+"?getByDate="+dateString+""))!

        case .medicalRecordShareWith:
            return URL(string: String(baseURLString+"medicalRecords/shareWith"))!
            
        case .getRecordFiles(let medical_record_id):
            return URL(string: String(baseURLString+"recordFiles?medical_record="+medical_record_id+"&limit=30&skip=0"))!
            
        case .uploadRecordFile:
            return URL(string: String(baseURLString+"recordFiles"))!
            
        case .medicalRecords:
            return URL(string: String(baseURLString+"medicalRecords?limit=30&skip=0"))!
            
        case .medicalRecordsAll:
            return URL(string: String(baseURLString+"medicalRecords?getAll=1&limit=30&skip=0"))!
            
        case .createMedicalRecords:
            return URL(string: String(baseURLString+"medicalRecords"))!
            
        case .getShareDocs:
            return URL(string: String(baseURLString+"medicalRecords/shareDocs?limit=1000&skip=0"))!
            
        case .patient_list:
            return URL(string: String(baseURLString+"user/patient_list?limit=1000&skip=0"))!
            
        case .doctorReviewGet(let doctor_id):
            return URL(string: String(baseURLString+"userReview?user_id="+doctor_id+"&limit=300&skip=0"))!
            
        case .myTransactions:
            return URL(string: String(baseURLString+"user/myTransactions?limit=100&skip=0"))!
            
        case .notification:
            return URL(string: String(baseURLString+"notification?limit=1000&skip=0"))!
            
        case .change_password:
            return URL(string: String(baseURLString+"user/change_password"))!
            

        case .update_profile:
            return URL(string: String(baseURLString+"user/update_profile"))!

            
        case .resendOTP:
            return URL(string: String(baseURLString+"auth/resendOtp"))!
            
        case .getNearest(let lattitude, let longitude):
            return URL(string: String(baseURLString+"districttown/getNearest?location="+lattitude+"&location="+longitude+""))!
            
        case .specialization:
            return URL(string: String(baseURLString+"specialization"))!
            
        case .register:
            return URL(string: String(baseURLString+"doctor/register"))!
            
        case .verifyOTP:
            return URL(string: String(baseURLString+"auth/verifyOtp"))!
            
        case .checkUserExist:
            return URL(string: String(baseURLString+"auth/checkUserExist"))!
            
        case .changeMobileNumber:
            return URL(string: String(baseURLString+"auth/changeMobileNumber"))!
            
        case .forgotPassword:
            return URL(string: String(baseURLString+"auth/forgotPassword"))!
            
        case .resetPassword:
            return URL(string: String(baseURLString+"auth/resetPassword"))!
            
        case .verifyForgotOtp:
            return URL(string: String(baseURLString+"auth/verifyForgotOtp"))!
            
        case .articles:
            return URL(string: String(baseURLString+"article"))!
            
        case .categoryList:
            return URL(string: String(baseURLString+"category"))!
            
        case .articleListByCat(let catID):
            return URL(string: String(baseURLString+"category/"+catID))!
            
        case .articleLike:
            return URL(string: String(baseURLString+"article/like"))!
            
        case .articleView:
            return URL(string: String(baseURLString+"article/view"))!

        case .articleBookMark:
            return URL(string: String(baseURLString+"article/bookmark"))!
            
        case .get_comment(let article_id):
            return URL(string: String(baseURLString+"article/get_comment?article_id="+article_id))!
            
        case .search(let searchString, let categoriesID):
            return URL(string: String(baseURLString+"article/search?categories="+categoriesID+"&q="+searchString+"&limit=300&skip=0"))!

        case .appointmentList(let historyStr, let upcomingStr):
            return URL(string: String(baseURLString+"appointments?getHistory="+historyStr+"&getUpcoming="+upcomingStr+"&limit=300&skip=0"))!

        case .comment:
            return URL(string: String(baseURLString+"article/comment"))!
            
        case .myLikes:
            return URL(string: String(baseURLString+"article/myLikes"))!
            
        case .myBookmark:
            return URL(string: String(baseURLString+"article/myBookmark"))!
            
        case .rejectAppointment:
            return URL(string: String(baseURLString+"appointments/reject"))!
            
        case .acceptAppointment:
            return URL(string: String(baseURLString+"appointments/accept"))!
            
        case .addToWallet:
            return URL(string: String(baseURLString+"add-to-Wallet"))!
        
        case .profile:
            return URL(string: String(baseURLString+"user/profile"))!
        case .getDoctorWorkingAddress(_):
             return URL(string: String(baseURLString+"add-to-Wallet"))!
        }
    }
}
    
//MARK:- Enum Endpoint Final Path
enum LinksEnum : String
{
    case privacyPolicy = "http://202.157.76.19:15060/privacy-policy"
    case termsAndCond = "http://202.157.76.19:15060/terms-and-conditions"
    case whySignUp = "http://202.157.76.19:15060/why-sign-up"
    
    case visitOurWebsite = "http://202.157.76.19:15060/"
    case contactUs = "http://202.157.76.19:15060/contact-us"
    case patientRights = "http://202.157.76.19:15060/patient-rights"
    case doctorRights = "http://202.157.76.19:15060/doctor-rights"
    case complaintPolicy = "http://202.157.76.19:15060/complaint-policy"
    case cancellationPolicy = "http://202.157.76.19:15060/cancellation-policy"
    case FAQs = "http://202.157.76.19:15060/faq"
    
    case cromeUrl = "http://www.ooe.com"
    case facebookUrl = "http://www.goe.com"
    case twitterUrl = "http://www.goo.com"
}

//Statics Pages web URL:-

//Staging Environment
//- Website                      => http://202.157.76.19:15060/
//- Contact Us                 => http://202.157.76.19:15060/contact-us
//- Privacy Policy             => http://202.157.76.19:15060/privacy-policy
//- T & C                         => http://202.157.76.19:15060/terms-and-conditions
//- Patient Rights            => http://202.157.76.19:15060/patient-rights
//- Doctor Rights            => http://202.157.76.19:15060/doctor-rights
//- Complaint Policy       => http://202.157.76.19:15060/complaint-policy
//- Cancellation Policy    => http://202.157.76.19:15060/cancellation-policy
//- F A Q                         => http://202.157.76.19:15060/faq


//Development Environment:-
//- Website                      => http://192.168.0.233:4200/
//- Contact Us                 => http://192.168.0.233:4200/contact-us
//- Privacy Policy             => http://192.168.0.233:4200/privacy-policy
//- T & C                         => http://192.168.0.233:4200/terms-and-conditions
//- Patient Rights            => http://192.168.0.233:4200/patient-rights
//- Doctor Rights            => http://192.168.0.233:4200/doctor-rights
//- Complaint Policy       => http://192.168.0.233:4200/complaint-policy
//- Cancellation Policy    => http://192.168.0.233:4200/cancellation-policy
//- F A Q                         => http://192.168.0.233:4200/faq

//Now it's only on Development Environment when we upload code then check on staging Environment
